<?php
/**
 * @file
 * Documentation for simplestep API.
 *
 * By reusing existing forms and breaking them down into a sequence (or wizard)
 * of smaller steps, Simplestep is a way to reduce the power of Drupal's UI down
 * to one decision at a time. It was originally designed to walk absolute
 * beginner webmasters through the choices needed to get them up and running on
 * building their first websites. Soon after, it was put to use creating a
 * workflow for some complex tasks that site admins need to get right without
 * having to memorise all the steps. You can use it to make a wizard for any
 * workflow that requires one or more forms from core, contrib or your own
 * custom modules.
 *
 * A simplestep sequence consists of a number of existing Drupal forms, which
 * are each to be displayed in part or in whole as one step in the sequence,
 * with added buttons allowing each one to be saved and marked as 'completed',
 * or marked as 'skipped' over without saving.  It is possible for a form to be
 * used in more than one step, with a different part of the form visible each
 * time.
 *
 * To define a simplestep sequence, or wizard, the minimum that a module must
 * implement is hook_simplestep_sequence_data(), and any callbacks it requires.
 * As much as possible, simplestep takes care of modifying outgoing links and
 * form redirects to ensure that the user remains in the sequence even amidst
 * complex behaviors.  In some cases, however, additional hook_form_alter()
 * changes will also be needed to achieve the required wizard functionality or
 * to override the form's theme function with a specialised one.
 *
 * Useful functions available to modules implementing simplestep sequences:
 * - simplestep_get_active(): Any hook_form_alter functions you write may call
 *   $simplestep = simplestep_get_active();
 *   and examine the contents of $simplestep['sequence']['id'] and
 *   $simplestep['form_id'] to establish whether they are modifying a step in
 *   the sequence, as opposed to the standard version of the form (which will
 *   probably be best left un-altered).
 * - simplestep_sequence_remaining_steps_to_do(): Returns boolean TRUE if any
 *   steps in the sequence are not marked 'completed', 'skipped' or 'n/a'.
 */

/**
 * Defines a set of one or more simplestep sequences and the steps they contain.
 *
 * @return array
 *   An associative array of arrays, keyed by strings identifying each sequence.
 *   Each sequence array has the following keys:
 *   - 'id': A string matching the key identifying this sequence.  Only required
 *     if this hook implementation calls any simplestep functions itself.
 *   - 'name': A human-readable name for the sequence, to be used when referring
 *     to it in lists, menus, etc.  Should be passed through t() as usual.
 *   - 'name_untranslated': A human-readable name for the sequence that will be
 *     used in menu definitions and which therefore will be passed through t()
 *     later and so should *not* be passed through t() now.
 *   - 'title': Optional.  A fuller title for the sequence, to be shown on its
 *     own introduction and summary page.
 *   - 'description'.  A description of the purpose of this sequence, to be
 *     shown on the introduction and summary page.
 *   - 'completed_description': Optional.  A text to replace the description on
 *     the introduction and summary page once the sequence has been completed.
 *   - 'advanced_links': Optional.  An array of links etc to be shown within the
 *     'advanced options' section of the introduction and summary page.  It may
 *     necessary to pass the rest of this array to a simplestep function before
 *     populating this part of the array (see example below).
 *   - 'path': A menu path by which this sequence is to be accessed.
 *   - 'path_type': Optional.  A value for 'type' in a hook_menu array, defining
 *     the sequence's path's behavior in menus.  Defaults to MENU_CALLBACK.
 *   - 'menu_name': Optional.  The menu into which this link should go, if the
 *     path_type indicates a menu link.
 *   - 'menu_plid': Optional.  The ID of the parent link in the menu under which
 *     this link is to be added.
 *   - 'menu_weight': Optional.  The weight that this link menu should have.
 *   - 'access_arguments': Optional.  An array of strings identifying
 *     permissions which should all be held by the user in order to access the
 *     sequence and its steps, in addition to 'use_simplestep'.
 *   - 'steps': An array of arrays, each one keyed by a string identifying a
 *     step in the sequence, and itself containing the following keys to define
 *     the step and identify the existing form that it is based on:
 *     - 'name': A human readable name (wrapped in t()) for this step.
 *     - 'form_url': The path where the existing form for this step is found. It
 *       may contain tokens (beginning with '%') that can be defined in
 *       hook_simplestep_token_values().
 *     - 'form_id': A string, or an array of strings, identifying the ID(s) of
 *       the form(s) that are displayed by this form_url and are to be modified
 *       within this step.  Multiple form IDs can apply for one form_url when
 *       a form interface has multiple stages.  If you declare multiple form IDs
 *       here, many other settings may be given as multidimensional arrays,
 *       keyed by form ID first, in order only to apply to that specific form -
 *       these settings are annotated "May be declared per-form-id".  You are
 *       likely to want to use that format in combination with 'wait_until_done'
 *       and/or 'no_wizard_buttons' on some forms within the step.
 *     - 'form_elements': Optional.  An array of form elements to be shown in
 *       this step.  A form tree path may be used to identify an element, and
 *       expressed in the format 'search_fieldset][search_form][search'.
 *       Standard Drupal form elements, including the primary submit button,
 *       will be included automatically.  If this value is omitted, all elements
 *       in the form will be shown.  (May be declared per-form-id.)
 *     - 'exclude_form_elements': Optional.  Instead of specifying elements to
 *       show, an array of form elements may be specified here, and they alone
 *       will be hidden.  (May be declared per-form-id.)
 *     - 'instructions':  Optional.  Additional text (wrapped in t()) to be
 *       displayed at the top of the form when it is shown within the sequence.
 *     - 'wait_until_done': Optional.  A boolean value.  If set to TRUE, instead
 *       of replacing the submit button with a 'Save and continue' button, the
 *       submit button is retained and serves to submit the form as usual, while
 *       an additional 'Save and continue' button is added which both submits
 *       the form and proceeds to the next step in the sequence.  (May be
 *       declared per-form-id.)
 *     - 'explicit_redirect': Optional.  A boolean value.  In some cases, forms
 *       which trigger batch processing may need this value to be set to TRUE in
 *       order for the simplestep sequence to resume once the batch processing
 *       is complete.  This setting may become unnecessary following further
 *       refinement of this module.  (May be declared per-form-id.)
 *     - 'do_not_rename_submit': Optional.  If TRUE, the form's existing submit
 *       button is not relabelled to 'Save and continue'.  (May be declared
 *       per-form-id.)
 *     - 'no_skip_button': Optional.  A boolean value.  If TRUE, then the step
 *       is displayed without a 'skip this step' button, to strongly encourage
 *       the user to complete it.  However, subsequent steps can still be
 *       accessed directly via the links in the progress summary, so to really
 *       enforce a step one would need to implement a callback that checks
 *       whether the required step had been completed, and use that callback
 *       with 'not_applicable_if' on all other steps.  (May be declared
 *       per-form-id.)
 *     - 'no_wizard_buttons': Optional.  If TRUE, the form is displayed without
 *       any simplestep control buttons.  This is most useful in a per-form-id
 *       situation, when submitting the form will result in another form being
 *       shown under the same URL and that form does include simplestep buttons,
 *       or when two or more (sub-)forms are visible on the page at once but
 *       only one of them should have the wizard buttons added.  (May be
 *       declared per-form-id.)
 *     - 'suppress_menus':  Optional.  If the form is usually displayed with
 *       primary and secondary tabs, such as to display local tasks and
 *       different contexts for settings, this setting can be used to hide those
 *       tab menus.  (May be declared per-form-id.)
 *     - 'skip_if_not_present':  Optional.  An array of form elements which must
 *       be present in the form (as generated by existing code) otherwise
 *       simplestep will mark it 'not applicable' and skip over it.  Items in
 *       this array are combined by OR.  Step will be skipped if any one of them
 *       is not present.  To skip step only if several items are ALL missing,
 *       group them inside an inner array.  For example:
 *         'skip_if_not_present' => array(
 *           array('this', 'or][this'), // need to see at least one of these
 *           'required][element'        // and need to see this
 *         )
 *       (May be declared per-form-id.)
 *     - 'not_applicable_if': Optional.  A string containing the name of a
 *       callback function which will be evaluated when the step is accessed
 *       either directly or because it is next in sequence.  If the callback
 *       function returns non-empty (e.g. TRUE), then this step will be marked
 *       'not applicable' and skipped over, with a message shown to the user.
 *       (May NOT be declared per-form-id.)
 *     - 'completed_if': Optional.  Similar to 'not_applicable_if', but if the
 *       callback function returns non-empty then the step will be marked
 *       'completed' and skipped over, and a message indicating successful
 *       'completion is shown to the user.  (May NOT be declared per-form-id.)
 *     - 'upon_recompletion_reset_steps':  Optional.  An array of sequence step
 *       IDs.  Whenever the current step is marked or re-marked 'completed', the
 *       steps specified here will be marked as 'to do', regardless of whether
 *       they were previously marked 'not applicable' or 'completed'.  Typically
 *       this would be used for steps that come after the current step and whose
 *       contents or available options depend on the setting of the current
 *       step.  Combining with 'completed_if' is probably not advisable.  (May
 *       NOT be declared per-form-id.)
 *     - 'overrides': Optional.  An array, keyed by form_id, of arrays, keyed by
 *       form tree path, of arrays of override values keyed by property name
 *       (without the leading '#').  Values of any form attribute may be
 *       overridden using this setting.  The form tree path is a string such as
 *       'node][taxonomy][term'.  (NB: MUST be declared per-form-id.)
 */
function hook_simplestep_sequence_data() {
  $sequences = array(
    'initial_setup' => array(
      'id' => 'initial_setup',
      'name' => t('Initial setup'),
      'name_untranslated' => 'Initial setup',
      'title' => t('Welcome to the site builder'),
      'description' => t('Follow these steps to get your website up and running and looking the way you want it.'),
      'completed_description' => t('You have completed the initial setup of your website.  Click any of the headings to go back and revise them, or click the button below to proceed to create content.'),
      'advanced_links' => array(), // See additions to this, below.
      'path' => 'initial_setup',
      'path_type' => MENU_CALLBACK,
      'menu_name' => 'management',
      'menu_plid' => 1,
      'menu_weight' => -100,
      'access_arguments' => array('use initial_setup'),
      'steps' => array(
        // This is the minimum required to define one step in a sequence.
        'sitename' => array(
          'name' => t('Website name'),
          'form_url' => 'admin/config/sitename_slogan',
          'form_id' => 'wildfire_i18n_sitename_slogan_form',
        ),
        'theme' => array(
          'name' => t('Theme'),
          // Use a simplified default theme selection form that uses radios.
          'form_url' => 'admin/setup/theme',
          'form_id' => 'select_theme_form',
          'upon_recompletion_reset_steps' => array(
            'background',
            'color',
          ),
        ),
        'background' => array(
          'name' => t('Background image'),
          // See hook_simplestep_token_values().
          'form_url' => 'admin/appearance/settings/%initial_setup_current_theme',
          'form_id' => 'system_theme_settings',
          'form_elements' => array(
            'var',
            'background_image',
          ),
          'overrides' => array(
            'system_theme_settings' => array(
              'background_image][background_image_image_fieldset][background_image_upload' => array(
                'description' => t("Click 'Browse...' to select a file on your computer for upload. Allowed filename extensions: @extensions.<br />The selected file will be uploaded when you click 'Save and continue'.", array('@extensions' => 'png gif jpg jpeg'))),
              ),
              'background_image' => array(
                'type' => 'container',
              ),
            ),
          ),
          // In this case, the primary tabs show other local tasks for
          // appearance settings and the secondary tabs show the themes for
          // which the settings can be adjusted.  Since we have already ensured
          // that this step covers the currently set theme, we can do without
          // both sets of tabs.
          'suppress_menus' => array(
            'primary',
            'secondary',
          ),
          // Not all themes support a background_image setting, so this step
          // should be skipped if no such setting is currently part of the
          // theme_settings form.
          'skip_if_not_present' => array(
            'background_image',
          ),
        ),
        'color' => array(
          'name' => t('Color'),
          'form_url' => 'admin/appearance/settings/%initial_setup_current_theme',
          'form_id' => 'system_theme_settings',
          'form_elements' => array(
            'color',
          ),
          'suppress_menus' => array(
            'primary',
            'secondary',
          ),
          'overrides' => array(
            'system_theme_settings' => array(
              'color' => array(
                'type' => 'container',
              ),
            ),
          ),
        ),
        'users' => array(
          'name' => t('Add or remove users'),
          'instructions' => t('These users may have been imported when this site was cloned.  If they are not relevant to this site, you should delete them now.  You may also add new users on from this screen.'),
          'form_url' => 'config/admin/users',
          'form_id' => array('user_admin_account', 'user_filter_form', 'user_multiple_cancel_confirm', 'user_register'),
          'exclude_form_elements' => array(
            'user_multiple_cancel_confirm' => array('user_cancel_confirm', 'user_cancel_notify'),
          ),
          'no_wizard_buttons' => array(
            'user_filter_form' => TRUE,
            'user_multiple_cancel_confirm' => TRUE,
          ),
          'wait_until_done' => TRUE,
          'explicit_redirect' => TRUE,
        ),
        // This step uses the permissions grid for anonymous users, but a custom
        // theme function (inserted using hook_form_alter) hides all but one of
        // the permissions.
        'public_access' => array(
          'name' => t('Revoke public access to website pages'),
          'form_url' => 'admin/people/permissions/1',
          'form_id' => 'user_admin_permissions',
          'suppress_menus' => array(
            'primary',
            'secondary',
          ),
          'not_applicable_if' => 'initial_setup_no_public_access',
        ),
      ),
    ),
  );

  // Add an extra setting to the advanced section of the sequence summary page,
  // giving the option to show this wizard upon login in future.  (Other
  // functions, not included in this example, would implement that behavior, but
  // this is where the setting is controlled.)
  $steps_remaining = simplestep_sequence_remaining_steps_to_do($sequences['initial_setup']);
  $show_wizard = variable_get('initial_setup_show_wizard', NULL);
  $sequences['initial_setup']['advanced_links']['show_wizard_fieldset'] = array(
    '#type' => 'fieldset',
    '#attributes' => array('class' => array('simplestep-submit-button')),
  );
  $sequences['initial_setup']['advanced_links']['show_wizard_fieldset']['show_wizard'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show this wizard when I log in'),
    // If this setting has previously been explicitly set, use that value, else
    // default to a sensible value: checked if there are steps remaining,
    // unchecked if not.
    '#default_value' => is_null($show_wizard) ? $steps_remaining : $show_wizard,
  );
  $sequences['initial_setup']['advanced_links']['show_wizard_fieldset']['show_wizard_submit'] = array(
    '#type' => 'submit',
    '#submit' => array(
      // The submit function saves the show_wizard flag variable and decides
      // whether to do a redirect based on the setting, now that the wizard is
      // completed.
      'initial_setup_show_wizard_submit',
      'simplestep_sequence_intro_form_submit'
    ),
    '#value' => t('Save and continue'),
    '#attributes' => array('class' => array('simplestep-submit-button')),
  );
  // Provide an additional 'advanced link' based on admin permissions.
  if (user_access('administer site configuration')) {
    $sequences['initial_setup']['advanced_links']['admin'] = array(
      'url' => 'admin',
      'text' => t('Administration menu'),
    );
  }
  return $sequences;
}



/**
 * TODO
 */
function hook_simplestep_token_values() {
  return array(
    '%initial_setup_current_theme' => variable_get('theme_default','none'),
  );
}

/**
 * Implements hook_theme().
 *
 * NB This is not part of the simplestep API but is included here as part of the
 * illustrative example.  This is needed in order to use a custom theme with a
 * form that is a step in the sequence.
 */
function initial_setup_theme() {
  return array(
    'initial_setup_user_admin_permissions_form' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Implements hook_form_alter().
 *
 * NB This is not part of the simplestep API but is included here as part of the
 * illustrative example.  This is needed in order to use a custom theme with a
 * form that is a step in the sequence.
 */
function initial_setup_form_alter(&$form, &$form_state, $form_id) {
  $simplestep = simplestep_get_active();
  if (!empty($simplestep) && $simplestep['sequence']['id'] == 'initial_setup') {
    switch($form_id) {
      case 'user_admin_permissions' :
        $form['#theme'] = array('initial_setup_user_admin_permissions_form');
    }
  }
}

/**
 * Theme the user_admin_permissions form, when part of this simplestep sequence.
 *
 * Make only one checkbox visible and ensure other permissions are unchanged.
 * (It may have been possible to achieve this by prefilling the filter criteria
 * with  perm:"view published content"  rather than a custom theme function.)
 */
function theme_initial_setup_user_admin_permissions_form($variables) {
  // In real life, use a CSS file rather than inline code.
  drupal_add_css('div.hide {display: none;}', 'inline');

  $form = $variables['form'];
  $active_simplestep = simplestep_get_active();
  $output = '<p>' . t('To revoke public access to pages on this website, uncheck this box and save the setting.') . '</p>';
  $row = array(drupal_render($form['permission']['access content']), drupal_render($form['checkboxes'][1]['access content']));
  $output .= theme('table', array('rows' => array($row), 'header' => array('', t('Anonymous user'))));
  $output .= drupal_render($form['actions']);
  $output .= '<div class="hide">' . drupal_render_children($form) . '</div>';
  return $output;
}


/**
 * Callback function used in 'public_access' step of 'initial_setup' sequence.
 *
 * Establish whether anonymous users already have no access to view content.
 *
 * @return bool
 *   TRUE if the user access permission for 'access content' is FALSE for
 *   anonymous users, FALSE otherwise.
 */
function initial_setup_no_public_access() {
  static $result;
  if (!isset($result)) {
    $result = !user_access('access content', drupal_anonymous_user());
  }
  return $result;
}